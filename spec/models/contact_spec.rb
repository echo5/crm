require 'rails_helper'

RSpec.describe Contact, type: :model do
  before do
    @contact = create(:contact)
  end

  describe '.name' do
    context 'when no first or last name' do
      it 'is nil' do
        @contact.first_name = nil
        @contact.last_name = nil
        expect(@contact.name).to eq (nil)
      end
    end
    context 'when first and last name' do
      it 'has full name' do
        expect(@contact.name).to eq ('John Doe')
      end
    end
    context 'when only first name' do
      it 'has only first name' do
        @contact.last_name = nil
        expect(@contact.name).to eq ('John')
      end
    end
  end

end
