FactoryBot.define do

  factory :user do
    email "admin@example.com"
    password "password"
  end

  factory :contact do
    first_name "John"
    last_name "Doe"
    email "joe@gmail.com"
  end

end