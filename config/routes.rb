Rails.application.routes.draw do
  resources :calls do
    post :generate_token, on: :collection
    post :get_caller, on: :collection
  end
  resources :contact_segments
  resources :deals
  devise_for :users
  resources :contact_sequences
  resources :sequences do
    resources :sequence_steps
  end
  resources :emails do
    post :send_email, on: :collection
    get :open, on: :member
    get :click, on: :member
    get :opt_out, on: :member
    get :opt_in, on: :member
  end
  resources :companies do
    post :import, on: :collection
  end
  resources :contacts do
    post :import, on: :collection
    resources :contact_notes
  end
  resources :contact_tags

  get 'search', to:'search#index'
  get 'inbox', to: 'inbox#index'

  scope 'settings' do
    resources :email_accounts
    resources :email_templates
    resources :api_keys    
    resources :users    
    resources :custom_fields    
    resources :stages    
    get 'import', to: 'settings#import'
  end

  namespace :api do
    namespace :v1 do
      resources :contacts
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: "contacts#index"
end
