module EmailTracking
  class Interceptor
    class << self
      def delivering_email(message)
        EmailTracking::Processor.new(message, self).track_send
      end
    end
  end
end
