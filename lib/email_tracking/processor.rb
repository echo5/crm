
module EmailTracking
  class Processor
    attr_reader :message, :mailer, :email

    def initialize(message, mailer = nil)
      @message = message
      @mailer = mailer
      abort @message.email.inspect
    end
    
    def process
      @email = Email.new
      email.token = generate_token
      track_open if options[:open]
      track_links if options[:click]
      email.save!
    end

    def track_send
      # if message.perform_deliveries
      #   email = Email.where(id: message_id.to_s).first
      #   if email
      #     email.sent_at = Time.now
      #     email.save
      #   end
      #   message["Ahoy-Message-Id"] = nil
      # end
    end

    protected
    
      def generate_token
        SecureRandom.urlsafe_base64(32).gsub(/[\-_]/, "").first(32)
      end

      def track_open
        if html_part?
          raw_source = (message.html_part || message).body.raw_source
          regex = /<\/body>/i
          url =
            url_for(
              controller: "emails",
              action: "open",
              id: @email.token,
              format: "gif"
            )
          pixel = ActionController::Base.helpers.image_tag(url, size: "1x1", alt: "")

          # try to add before body tag
          if raw_source.match(regex)
            raw_source.gsub!(regex, "#{pixel}\\0")
          else
            raw_source << pixel
          end
        end
      end

      def track_links
        if html_part?
          body = (message.html_part || message).body

          doc = Nokogiri::HTML(body.raw_source)
          doc.css("a[href]").each do |link|
            uri = parse_uri(link["href"])
            next unless trackable?(uri)

            if options[:click] && !skip_attribute?(link, "click")
              signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new("sha1"), ENV['SECRET_KEY_BASE'], link["href"])
              link["href"] =
                url_for(
                  controller: "emails",
                  action: "click",
                  id: @email.token,
                  url: link["href"],
                  signature: signature
                )
            end
          end

          # hacky
          body.raw_source.sub!(body.raw_source, doc.to_s)
        end
      end

      def html_part?
        (message.html_part || message).content_type =~ /html/
      end

      def skip_attribute?(link, suffix)
        attribute = "data-skip-#{suffix}"
        if link[attribute]
          # remove it
          link.remove_attribute(attribute)
          true
        elsif link["href"].to_s =~ /unsubscribe/i && !options[:unsubscribe_links]
          # try to avoid unsubscribe links
          true
        else
          false
        end
      end

      # Filter trackable URIs, i.e. absolute one with http
      def trackable?(uri)
        uri && uri.absolute? && %w(http https).include?(uri.scheme)
      end

      # Parse href attribute
      # Return uri if valid, nil otherwise
      def parse_uri(href)
        # to_s prevent to return nil from this method
        Addressable::URI.parse(href.to_s) rescue nil
      end

      # def url_for(opt)
      #   opt = (ActionMailer::Base.default_url_options || {})
      #         .merge(options[:url_options])
      #         .merge(opt)
      #   AhoyEmail::Engine.routes.url_helpers.url_for(opt)
      # end
  end
end