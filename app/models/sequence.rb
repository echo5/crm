class Sequence < ApplicationRecord
  belongs_to :email_account
  has_many :emails
  has_many :contact_sequences
  has_and_belongs_to_many :segments, class_name: 'ContactSegment'
  has_many :contacts, through: :contact_sequences
  has_many :steps, class_name: 'SequenceStep', inverse_of: :sequence
  has_one :root_step, -> { where parent_id: nil }, class_name: 'SequenceStep', inverse_of: :sequence
  has_many :hours, as: :hourable, inverse_of: :hourable
  has_many :open_hours, -> { where(open: true) }, as: :hourable, inverse_of: :hourable, class_name: 'Hour'
  accepts_nested_attributes_for :steps, :hours, :contact_sequences
  after_save :run_contact_sequences

  def open_rate
    total = emails.count
    opened = emails.distinct.joins(:opens).count
    opened.to_f / total.to_f
  end

  private

    def run_contact_sequences
      if active?
        contact_sequences.each do |contact_sequence|
          contact_sequence.run_sequence
        end
      end
    end


end
