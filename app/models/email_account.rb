require 'net/imap'
require 'resolv-replace'

class EmailAccount < ApplicationRecord
  before_create :set_latest_uid

  def latest_uid
    imap = Net::IMAP.new(imap_host, imap_port.to_i, true, nil, false)
    imap.login(imap_username, imap_password)
    imap.examine('INBOX')
    imap.uid_fetch("*", 'RFC822')[0].attr['UID']
  end

  def sync_emails
    imap = Net::IMAP.new(imap_host, imap_port.to_i, true, nil, false)
    imap.login(imap_username, imap_password)
    imap.examine('INBOX')
    imap.uid_search("#{self.last_uid_checked}:*").each do |uid|
      msg = imap.fetch(uid,'RFC822')[0].attr['RFC822']
      mail = Mail.read_from_string msg
      self.last_uid_checked = uid      
      contact = Contact.find_by_email([mail.from[0], mail.to[0]])
      if contact
        Email.create(
          contact_id: contact.id,
          header: mail.header,
          date: mail.date,
          subject: mail.subject,
          body: mail.body,
          from: mail.from[0],
          to: mail.to[0],
          created_at: mail.date.to_s
        )
      end
    end
    self.save
  end

  def self.sync_all
    all.each do |account|
      account.sync_emails
    end
  end

  def send_email_template(contact, email_template)
    template = Liquid::Template.parse(email_template.content)
    output = template.render('contact' => contact.attributes, :error_mode => :strict)
    self.send_email(contact.email, email_template.subject, output)
  end

  private 
    def set_latest_uid
      self.last_uid_checked = self.latest_uid
    end
  
end
