class ContactSegment < ApplicationRecord
  has_many :conditions, as: :conditionable
  has_and_belongs_to_many :contacts
  has_and_belongs_to_many :sequences  
  accepts_nested_attributes_for :conditions, reject_if: :all_blank, allow_destroy: true
  before_save :reevalute_contacts

  def evaluate(contact)
    result = false
    conditions.each_with_index do |condition, index|
      if condition.logical_operator == 'OR' || index == 0
        result = result || condition.evaluate(contact)
      else
        result = result && condition.evaluate(contact)
      end        
    end
    result
  end

  private
    def reevalute_contacts
      self.contacts = []
      all_contacts = Contact.all
      all_contacts.each do |contact|
        if self.evaluate(contact)
          self.contacts << contact
        end
      end
    end
end
