class CustomField < ApplicationRecord
  validates_presence_of :label, :name, :field_type, :model
  validates :name, format: { with: /\A[a-zA-Z1-9\-_\/]+\Z/ }

  enum model: {
    Contact: 0,
  }

  enum field_type: {
    string: 0,
    text: 1,
    boolean: 2,
    radio_buttons: 3
  }

  def self.contact_fields
    where(model: 'Contact')
  end

  def options_array
    options.split("\r\n") if options
  end

end
