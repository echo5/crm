class Call < ApplicationRecord
  include Filterable  
  belongs_to :contact
  belongs_to :user, optional: true

  # Filter scopes
  default_scope { order(created_at: :desc) }
  scope :from_number, -> (from) { where('"from" like ?', "%#{from}%") }
  scope :to_number, -> (to) { where('"to" like ?', "%#{to}%") }
  scope :contact,-> (contact) { where(contacts: {id: contact}) }
  scope :owner, -> (owner) { where(contacts: {owner_id: owner}) }

  enum direction: {
    inbound: 0,
    outbound: 1
  }

  enum status: {
    'queued': 0,
    'ringing': 1,
    'in-progress': 2,
    'canceled': 3,
    'completed': 4,
    'failed': 5,
    'busy': 6,
    'no-answer': 7
  }

end
