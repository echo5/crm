class Email < ApplicationRecord
  include Filterable
  belongs_to :contact, :touch => :last_activity_at
  belongs_to :user, optional: true
  belongs_to :email_account
  belongs_to :sequence, optional: true
  has_many :opens, class_name: 'EmailOpen'
  accepts_nested_attributes_for :opens
  attr_accessor :track_open, :track_clicks, :in_reply_to

  # Validation
  validates_uniqueness_of :message_id
  validates_presence_of :subject, :contact_id, :email_account
  validates :email_template_id, uniqueness: { scope: :contact_id }, if: Proc.new { |e| !e.email_template_id.blank? && e.sequence_id }

  # Filter scopes
  default_scope { order(created_at: :desc) }
  scope :subject, -> (subject) { where('subject ~* ?', subject) }
  scope :owner, -> (owner) { where(contacts: {owner_id: owner}) }

  # Actions
  after_initialize :generate_token
  after_create :end_sequences_on_reply  
  before_save :filter_body_html

  enum direction: {
    inbound: 0,
    outbound: 1,
    outbound_sequence: 2,
  }

  def sender
    if user_id
      user
    else
      contact
    end
  end


  private

    def end_sequences_on_reply
      if from == contact.email
        sequence_ids = contact.sequences.where(end_on_reply: true).pluck(:id)
        contact.contact_sequences.where(id: sequence_ids).update_all(status: 'complete')
      end
    end

    def generate_token
      self.token ||= SecureRandom.urlsafe_base64(32).gsub(/[\-_]/, "").first(32)
    end

    def filter_body_html
      self.body = Sanitize.fragment(self.body, Sanitize::Config::RELAXED)
    end
end