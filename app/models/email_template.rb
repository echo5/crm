class EmailTemplate < ApplicationRecord
  has_many :sent_emails, class_name: 'Email'

  def open_rate
    total = sent_emails.count
    opened = sent_emails.distinct.joins(:opens).count
    opened.to_f / total.to_f
  end
end
