class ContactNote < ApplicationRecord
  belongs_to :user
  belongs_to :contact, :touch => :last_acitivity_at
end
