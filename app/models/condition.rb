class Condition < ApplicationRecord
  belongs_to :conditionable, polymorphic: true

  enum logical_operator: {
    AND: 0,
    OR: 1
  }

  enum comparison_operator: {
    ' == ': 0,
    ' > ': 1,
    ' < ': 2,
    ' != ': 3,
    ' include? ': 4,
    ' start_with? ': 5,
  }

  def evaluate(object)
    result = false
    if object.class.public_properties.include? property
      result = object.public_send(property).public_send(comparison_operator.strip, value)
    end
    result
  end

end
