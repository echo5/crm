class Hour < ApplicationRecord
  after_initialize :default_values
  belongs_to :hourable, polymorphic: true
  validates :day, :uniqueness => { :scope => [:hourable_type, :hourable_id] }
  
  enum day: {
    sun: 0,
    mon: 1,
    tue: 2,
    wed: 3,
    thu: 4,
    fri: 5,
    sat: 6,
  }

  def self.next_open_time(hours)
    schedule = Biz::Schedule.new do |config|
      config.hours = {}
      hours.each do |hour|
        config.hours[hour.day.to_sym] = { hour.start.strftime('%H:%M') => hour.end.strftime('%H:%M') }
      end
    end
    schedule.time(0, :hours).after(Time.now)
  end

  private
    def default_values
      unless persisted?
        self.start ||= "09:00"
        self.end ||= "17:00"
        self.open ||= true if self.day != 'sunday' && self.day != 'saturday'
      end
    end

end
