class User < ApplicationRecord
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  def avatar(size = 100, classes = '')
    '<img src="' + self.avatar_url(size) + '" alt="' + self.name.to_s + '" class="avatar ' + classes + '">'
  end

  def avatar_url(size = 100)
    if !self.email.nil?
      gravatar_id = Digest::MD5.hexdigest(self.email.downcase)
      "http://gravatar.com/avatar/#{gravatar_id}.png?s=#{size}&d=retro"
    else
      'http://www.gravatar.com/avatar/?d=retro'
    end
  end

end
