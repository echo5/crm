class Company < ApplicationRecord
  has_many :contacts
  has_many :deals, through: :contacts
  validates_uniqueness_of :name
  validates_presence_of :name  

  def avatar 
    '<span class="lnr lnr-apartment company-avatar avatar"></span>'
  end

  def self.import_csv(file)
    total_imported = 0
    errors = nil
    options =  { 
      chunk_size: 1000,
    }
    catch :import_error do
      SmarterCSV.process(file.tempfile, options) do |rows|
        rows.each do |row|
          company = Company.create(row)
          if company.errors.any?
            errors = 'Import failed at row #' + (total_imported + 1).to_s + ': '
            errors += company.errors.full_messages.join("<br/>")
            throw :import_error
          else
            total_imported += 1
          end
        end
      end
    end
    return {
      errors: errors,
      total_imported: total_imported
    }
  end
end
