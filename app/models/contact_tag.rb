class ContactTag < ApplicationRecord
  has_and_belongs_to_many :contacts
  validates_uniqueness_of :name
  
end
