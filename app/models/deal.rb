class Deal < ApplicationRecord
  belongs_to :stage
  belongs_to :contact
  belongs_to :owner, class_name: "User"
  has_paper_trail only: [:stage, :status]  

  enum status: {
    won: 0,
    lost: 1,
    unqualified: 2,
    no_response: 3,
  }
  
end
