class Stage < ApplicationRecord
  has_many :deals
  validates_presence_of :name
  
end
