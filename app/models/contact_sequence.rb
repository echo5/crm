class ContactSequence < ApplicationRecord
  belongs_to :step, class_name: 'SequenceStep', foreign_key: 'sequence_step_id', optional: true
  belongs_to :contact
  belongs_to :sequence
  before_create :set_first_step
  validates :contact_id, :uniqueness => { :scope => [:sequence_id] }  
  after_create :begin_sequence
  attr_accessor :path

  enum status: {
    running: 0,
    pending: 1,
    complete: 2,
  }

  def run_sequence
    if self.step && self.status != 'pending' && self.status != 'complete'
      do_current_step
    end
    self.save
  end

  def next_step(path = 0)
    self.step.children.where(path: self.path).first
  end

  def do_next_step
    self.status = 'running'    
    self.path = 0    
    if self.next_step
      self.step = next_step
      do_current_step
    else
      self.status = 'complete'
      self.save
    end
  end

  def do_current_step
    case self.step.action
      when 'send_email'
        self.status = 'pending'
        delivery_time = Hour.next_open_time(sequence.open_hours)
        delay(run_at: delivery_time).send_email_template(self.step.email_template_id)
      when 'condition'
        if contact.attributes.include? self.step.condition_subject
          calculator = Dentaku::Calculator.new
          outcome = calculator.evaluate('CONTAINS(value, subject)', {subject: self.step.condition_subject, value: self.step.condition_value})
          self.path = outcome ? 1 : 0
          do_next_step       
        end
      when 'delay'
        if ['days', 'hours', 'minutes'].include?(self.step.delay_unit)
          self.status = 'pending'
          delay(run_at: self.step.delay_value.to_i.send(self.step.delay_unit).from_now).do_next_step
        end
    end
    self.save
  end

  def send_email_template(email_template_id)
    email_template = EmailTemplate.find(email_template_id)
    @email = Email.new(email_account: sequence.email_account, contact: contact, subject: email_template.subject, body: email_template.content, sequence: sequence)
    mailer = ContactMailer.with(
      email: @email
    ).default.deliver
    @email.message_id = mailer.message_id
    @email.save
    do_next_step
  end

  private

    def set_first_step
      self.sequence_step_id = sequence.root_step.id
    end

    def begin_sequence
      run_sequence if sequence.active && !complete? && !pending?
    end

end
