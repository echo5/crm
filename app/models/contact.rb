class Contact < ApplicationRecord
  include Filterable  

  # Associations
  belongs_to :company, optional: true
  has_many :deals
  belongs_to :owner, class_name: 'User', optional: true
  has_many :notes, class_name: 'ContactNote', dependent: :destroy
  has_many :contact_sequences, dependent: :destroy
  has_many :sequences, through: :contact_sequences
  has_many :emails, dependent: :destroy
  has_many :email_opens, dependent: :destroy
  has_many :link_clicks, dependent: :destroy
  has_many :calls, dependent: :destroy
  has_and_belongs_to_many :segments, -> { distinct }, class_name: 'ContactSegment'
  has_and_belongs_to_many :tags, class_name: 'ContactTag'
  has_paper_trail only: [:lifecycle_stage, :owner_id]
  accepts_nested_attributes_for :tags, :link_clicks

  # Callbacks
  store_accessor :custom
  after_initialize :add_store_accessor
  after_save       :add_store_accessor
  after_save :add_to_segments
  after_save :trigger_sequence
  validates_presence_of :email
  validates_uniqueness_of :email
  validate :validate_custom_fields
  phony_normalize :mobile_phone, default_country_code: 'US'
  phony_normalize :work_phone, default_country_code: 'US'

  # Filter scopes
  default_scope { order(created_at: :desc) }
  scope :lifecycle_stage, -> (stage) { where(lifecycle_stage:stage) }
  scope :tags, -> (tags) { includes(:tags).where(contact_tags: {id: tags}) }
  scope :owner, -> (owner) { where(owner: owner) }

  enum lifecycle_stage: {
    prospect: 0,
    subscriber: 1,
    lead: 2,
    customer: 3,
    past_customer: 4,
    other: 5,
  }

  enum dnc_reason: {
    opted_out: 0,
    unqualified: 1,
    bounced: 2,
  }

  def name
    "#{first_name} #{last_name}".squish.presence || nil
  end

  def avatar(size = 100, classes = '')
    '<img src="' + self.avatar_url(size) + '" alt="' + self.name + '" class="avatar ' + classes + '">'
  end

  def avatar_url(size = 100)
    if !self.email.nil?
      gravatar_id = Digest::MD5.hexdigest(self.email.downcase)
      "http://gravatar.com/avatar/#{gravatar_id}.png?s=#{size}&d=retro"
    else
      "http://www.gravatar.com/avatar/?d=retro&size=#{size}"
    end
  end

  def recent_activities(limit = 50)
     email_opens_stream = email_opens.order(created_at: :desc).to_enum #:find_each
     link_stream = link_clicks.order(created_at: :desc).to_enum #:find_each
     version_stream = versions.order(created_at: :desc).to_enum #:find_each
     deals_stream = deals.order(created_at: :desc).to_enum #:find_each
     notes_stream = notes.order(created_at: :desc).to_enum #:find_each
     email_stream = emails.order(created_at: :desc).to_enum #:find_each
     ActivityAggregator.new([email_opens_stream, deals_stream, notes_stream, version_stream, email_stream, link_stream]).next_activities(limit)
  end

  def tag_names
    tags.pluck(:name)
  end

  def self.public_properties
    attribute_names + ['tag_names']
  end

  def self.import_csv(file)
    total_imported = 0
    errors = nil
    options =  { 
      chunk_size: 1000,
    }
    catch :import_error do
      SmarterCSV.process(file.tempfile, options) do |rows|
        # @TODO put into background worker
        rows.each do |row|
          company = Company.find_by(name: row[:company])
          if company
            row[:company_id] = company.id
            row.delete(:company)
          end
          row[:tag_ids] = []
          row[:tags].split(',').each do |tag|
            contact_tag = ContactTag.find_or_create_by(name: tag)
            row[:tag_ids] << contact_tag.id
          end
          row.delete(:tags)
          contact = Contact.new
          contact.attributes = row
          contact.save
          if contact.errors.any?
            errors = 'Import failed at row #' + (total_imported + 1).to_s + ': '
            errors += contact.errors.full_messages.join("<br/>")
            throw :import_error
          else
            total_imported += 1
          end
        end
        # @TODO bulk import with associations
        # Contact.import rows
      end
    end
    return {
      errors: errors,
      total_imported: total_imported
    }
  end

  private 

    def add_store_accessor
      CustomField.contact_fields.each do |field|
        singleton_class.class_eval {store_accessor :custom, field.name}
      end
    end

    def validate_custom_fields
      CustomField.contact_fields.each do |field|
        if field.required? && custom[field.name].blank?
          errors.add field.name, "must not be blank"
        end
      end
    end

    def add_to_segments
      ContactSegment.includes(:conditions).each do |segment|
        if segment.evaluate(self)
          self.segments << segment unless self.segments.include?(segment)
        else
          self.segments.delete(segment) if self.segments.include?(segment)
        end
      end
    end

    def trigger_sequence
      sequences = Sequence.includes(:segments).where(contact_segments: {id: self.segments.pluck(:id)})
      sequences.each do |sequence|
        ContactSequence.create(contact_id: self.id, sequence_id: sequence.id) unless self.sequences.include?(sequence)
      end
    end

end
