class SequenceStep < ApplicationRecord
  belongs_to :sequence
  belongs_to :parent, class_name: 'SequenceStep', optional: true
  has_many :children, class_name: 'SequenceStep', foreign_key: 'parent_id', dependent: :destroy
  has_many :contact_sequences, dependent: :nullify
  store_accessor :properties, 
    :tag, 
    :email_template_id, 
    :condition_subject, 
    :condition_operator, 
    :condition_value,
    :delay_value,
    :delay_unit

  enum delay_unit: {
    minutes: 0,
    hours: 1,
    days: 2,
  }
  enum condition_subject: {
    email_unopened: 0,
    email_opened: 1,
    asset_downloaded: 2,
    asset_not_downloaded: 3,
    page_visited: 4,
    page_not_visited: 5,
  }
  enum condition_operator: {
    equal_to: 0,
    greater_than: 1,
    less_than: 2,
    not_equal_to: 3,
    contains: 4,
    starts_with: 5,
  }
  enum action: {
    condition: 0,
    delay: 1,
    send_email: 2,
  }

  def description
    description = action.humanize + ' '
    case action
    when 'send_email'
      description += "\"#{email_template.name}\"" if !email_template.nil?
    when 'delay'
      description += delay_value + ' ' + delay_unit if delay_unit && delay_value
    end
    if action == 'condition'
      description += condition_subject
      description += condition_operator
      description += condition_value
    end
    description
  end

  def icon
    case action
      when 'send_email'
        'envelope'
      when 'delay'
        'hourglass'
      when 'condition'
        'question-circle'
    end
  end

  def email_template
    if self.email_template_id
      EmailTemplate.find(self.email_template_id)
    end
  end

end
