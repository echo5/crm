class ContactSegmentsController < ApplicationController
  before_action :set_contact_segment, only: [:show, :edit, :update, :destroy]

  # GET /contact_segments
  # GET /contact_segments.json
  def index
    @contact_segments = ContactSegment.all.page params[:page]
  end

  # GET /contact_segments/1
  # GET /contact_segments/1.json
  def show
  end

  # GET /contact_segments/new
  def new
    @contact_segment = ContactSegment.new
  end

  # GET /contact_segments/1/edit
  def edit
  end

  # POST /contact_segments
  # POST /contact_segments.json
  def create
    @contact_segment = ContactSegment.new(contact_segment_params)

    respond_to do |format|
      if @contact_segment.save
        format.html { redirect_to @contact_segment, notice: 'Contact segment was successfully created.' }
        format.json { render :show, status: :created, location: @contact_segment }
      else
        format.html { render :new }
        format.json { render json: @contact_segment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contact_segments/1
  # PATCH/PUT /contact_segments/1.json
  def update
    respond_to do |format|
      if @contact_segment.update(contact_segment_params)
        format.html { redirect_to @contact_segment, notice: 'Contact segment was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact_segment }
      else
        format.html { render :edit }
        format.json { render json: @contact_segment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contact_segments/1
  # DELETE /contact_segments/1.json
  def destroy
    @contact_segment.destroy
    respond_to do |format|
      format.html { redirect_to contact_segments_url, notice: 'Contact segment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact_segment
      @contact_segment = ContactSegment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_segment_params
      params.require(:contact_segment).permit(:name, :description, conditions_attributes: [:id, :logical_operator, :property, :comparison_operator, :value, :_destroy])
    end
end
