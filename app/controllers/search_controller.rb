class SearchController < ApplicationController

  # GET /search
  def index
    users = Contact.where("lower(first_name) LIKE ? OR lower(last_name) LIKE ? OR lower(email) LIKE ?", "%#{params[:q].downcase}%", "%#{params[:q].downcase}%", "%#{params[:q].downcase}%")
    companies = Company.where("lower(name) LIKE ? OR lower(website) LIKE ?", "%#{params[:q].downcase}%", "%#{params[:q].downcase}%")
    deals = Deal.where("lower(name) LIKE ?", "%#{params[:q].downcase}%")
    @results = users + companies + deals
  end


end
