class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  # before_action :set_paper_trail_whodunnit
  # after_action :flash_to_headers

  def flash_to_headers
    return unless request.xhr?
    response.headers['X-Message'] = flash_message
    response.headers["X-Message-Type"] = flash_type.to_s
    flash.discard
  end

  private

    def flash_message
      [:error, :warning, :notice, :success].each do |type|
          return flash[type] unless flash[type].blank?
      end
      return nil
    end

    def flash_type
      [:error, :warning, :notice, :success].each do |type|
          return type unless flash[type].blank?
      end
    end
end
