class SequenceStepsController < ApplicationController
  before_action :set_sequence, only: [:new, :create, :edit, :destroy]
  before_action :set_sequence_step, only: [:show, :edit, :update, :destroy]

  # GET /sequence_steps
  # GET /sequence_steps.json
  def index
    @sequence_steps = SequenceStep.all
  end

  # GET /sequence_steps/1
  # GET /sequence_steps/1.json
  def show
  end

  # GET /sequence_steps/new
  def new
    @sequence_step = SequenceStep.new
    @sequence_step.parent_id = params[:parent] if params[:parent]
    render partial: 'form'
  end

  # GET /sequence_steps/1/edit
  def edit
    render partial: 'form'
  end

  # POST /sequence_steps
  # POST /sequence_steps.json
  def create
    @sequence_step = @sequence.steps.build(sequence_step_params)
    @replaced_step = SequenceStep.find_by(parent_id: sequence_step_params[:parent_id].presence, path: sequence_step_params[:path])

    if @sequence_step.save
      if @replaced_step
        @replaced_step.parent_id = @sequence_step.id
        @replaced_step.save
      end
    else
      flash[:error] = @sequence_step.errors
    end
  end

  # PATCH/PUT /sequence_steps/1
  # PATCH/PUT /sequence_steps/1.json
  def update
    if @sequence_step.update(sequence_step_params)
      flash[:success] = 'Sequence step was successfully updated.'
    else
      flash[:error] = @sequence_step.errors
    end
  end

  # DELETE /sequence_steps/1
  # DELETE /sequence_steps/1.json
  def destroy
    @sequence_step.destroy
      flash[:success] = 'Sequence step was successfully destroyed.'
      redirect_to @sequence
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sequence_step
      @sequence_step = SequenceStep.find(params[:id])
    end

    def set_sequence
      @sequence = Sequence.find(params[:sequence_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sequence_step_params
      params.require(:sequence_step).permit(:parent_id, :path, :condition_subject, :condition_operator, :condition_value, :action, :object, :email_template_id, :delay_value, :delay_unit)
    end
end
