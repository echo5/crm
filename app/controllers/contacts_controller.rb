class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :edit, :update, :destroy]

  # GET /contacts
  # GET /contacts.json
  def index
    @contacts = Contact.all.filter(params.slice(:lifecycle_stage, :owner, :tags)).page params[:page]
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
    @email = Email.new
    @contact_note = ContactNote.new
  end

  # GET /contacts/new
  def new
    @contact = Contact.new
    # render partial: 'new'
    render :json => { :content => render_to_string('new', :layout => false) }
  end

  # GET /contacts/1/edit
  def edit
    render partial: 'edit'
  end

  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new(contact_params)
    if @contact.save
      flash[:success] = 'Contact was successfully created.'
      render :js => "Turbolinks.visit('#{contact_url(@contact)}')"
    else
      flash[:error] = @contact.errors.full_messages.join("<br>")
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    if @contact.update(contact_params)
      flash[:success] = 'Contact was successfully updated.'
      # render :js => "Turbolinks.visit('#{contact_url(@contact)}')"      
    else
      flash[:error] = @contact.errors.full_messages.join('<br>')
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact.destroy
    flash[:success] = 'Contact was successfully destroyed.'
    redirect_to contacts_url
  end

  # POST /contacts/import  
  def import
    import = Contact.import_csv(params[:contacts_import_file])
    if import[:total_imported] > 0
      flash[:success] = pluralize(import[:total_imported], 'contacts') + ' were successfully imported.'
    end
    if import[:errors]
      flash[:error] = import[:errors]
    end
    redirect_to contacts_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.includes(:notes, :emails).find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:title, :first_name, :last_name, :email, :company_id, :job_title, :address1, :address2, :city, :state, :zip, :country, :mobile_phone, :work_phone, :website, :lifecycle_stage, :industry, :source, :owner_id, custom: {}, tag_ids: [])
    end
end
