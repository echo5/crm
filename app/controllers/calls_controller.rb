class CallsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:create]  
  skip_before_action :verify_authenticity_token, only: [:create]
  before_action :set_call, only: [:show, :edit, :update, :destroy]
  before_action :load_credentials, only: [:create, :create, :generate_token]
  before_action :authenticate_twilio_request, only: [:create]

  # GET /calls
  # GET /calls.json
  def index
    @calls = Call.includes(:contact).filter(params.slice(:from_number, :to_number, :contact, :owner)).page params[:page]
  end

  # GET /calls/1
  # GET /calls/1.json
  def show
  end

  # GET /calls/new
  def new
    @call = Call.new
  end

  # GET /calls/1/edit
  def edit
  end

  # POST /calls
  # POST /calls.json
  def create
    @call = Call.new(
      sid: params[:CallSid],
      from: params[:From],
      to: params[:To],
      status: 'ringing'
    )
    if params[:phoneNumber]
      @call.contact = Contact.where('mobile_phone = ? OR work_phone = ?', params[:phoneNumber], params[:phoneNumber]).first
      @call.direction = 'outbound'
    else
      @call.contact = Contact.where('mobile_phone = ? OR work_phone = ?', params[:From], params[:From]).first
      @call.direction = 'inbound'      
    end

    @call.save

    twiml = Twilio::TwiML::VoiceResponse.new do | r |
      if params[:phoneNumber]
        r.dial caller_id: ENV['TWILIO_NUMBER'], number: params[:phoneNumber]      
      else
        r.dial(caller_id: params[:From], timeout: 20) do |d|
          d.client 'admin'
          d.number '+886905145072'
        end
        r.say "We aren't available to take your call now.  Please leave a message with your name and number and we'll get back to you as soon as possible.", voice: 'alice'
        r.record transcribe: true, transcribe_callback: 'http://twimlets.com/voicemail?Email=joshuatf@gmail.com'
      end
    end

    render xml: twiml.to_xml
  end

  # PATCH/PUT /calls/1
  # PATCH/PUT /calls/1.json
  def update
    respond_to do |format|
      if @call.update(call_params)
        format.html { redirect_to @call, notice: 'Call was successfully updated.' }
        format.json { render :show, status: :ok, location: @call }
      else
        format.html { render :edit }
        format.json { render json: @call.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /calls/get_caller
  def get_caller
    @caller = Contact.where('mobile_phone = ? OR work_phone = ?', params[:from], params[:from]).first
    render json: { name: @caller.name }
  end

  # POST /calls/generate_token  
  def generate_token
    role = 'admin'
    capability = Twilio::JWT::ClientCapability.new(@twilio_sid, @twilio_token)
    outgoing_scope = Twilio::JWT::ClientCapability::OutgoingClientScope.new(@application_sid, role)
    capability.add_scope outgoing_scope

    incoming_scope = Twilio::JWT::ClientCapability::IncomingClientScope.new(role)
    capability.add_scope incoming_scope

    render json: {token: capability.to_s}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_call
      @call = Call.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def call_params
      params.require(:call).permit(:contact_id, :to, :from, :caller_name, :duration, :status, :direction)
    end

    # Is this a request from twilio?
    def authenticate_twilio_request
      if twilio_req?
        return true
      else
        response = Twilio::TwiML::VoiceResponse.new do|r|
          r.hangup
        end
        render xml: response.to_s, status: :unauthorized
        false
      end
    end

    # Validate headers from twilio
    def twilio_req?
      validator = Twilio::Security::RequestValidator.new(@twilio_token)
      post_vars = params.reject { |k, _| k.downcase == k }
      twilio_signature = request.headers['HTTP_X_TWILIO_SIGNATURE']
      validator.validate(request.url, post_vars, twilio_signature)
    end

    def load_credentials
      @twilio_sid = ENV['TWILIO_ACCOUNT_SID']
      @twilio_token = ENV['TWILIO_AUTH_TOKEN']
      @twilio_number = ENV['TWILIO_NUMBER']
      @application_sid = ENV['TWIML_APPLICATION_SID']
    end
end
