module Api
  module V1
    class ContactsController < ApplicationController
      skip_before_action :verify_authenticity_token
      respond_to :json
      before_action :restrict_access
      before_action :set_contact, only: [:show, :edit, :update, :destroy]

      # GET /contacts
      # GET /contacts.json
      def index
        @contacts = Contact.all
        render json: @contacts
      end

      # GET /contacts/1
      # GET /contacts/1.json
      def show
        render json: @contact
      end

      # POST /contacts
      # POST /contacts.json
      def create
        @contact = Contact.new(contact_params)
        if @contact.save
          render json: @contact, status: :created, location: @contact
        else
          render json: @contact.errors, status: :unprocessable_entity
        end
      end

      # PATCH/PUT /contacts/1
      # PATCH/PUT /contacts/1.json
      def update
        if @contact.update(contact_params)
          render :show, status: :ok, location: @contact
        else
          render json: @contact.errors, status: :unprocessable_entity
        end
      end

      # DELETE /contacts/1
      # DELETE /contacts/1.json
      def destroy
        @contact.destroy
        head :no_content
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_contact
          @contact = Contact.includes(:notes, :emails).find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def contact_params
          params.require(:contact).permit(:title, :first_name, :last_name, :email, :company_id, :job_title, :address1, :address2, :city, :state, :zip, :country, :mobile_phone, :work_phone, :website, :lifecycle_stage, :industry, :source, :owner_id, tag_ids: [])
        end
        
        def restrict_access
          authenticate_or_request_with_http_token do |token, options|
            ApiKey.exists?(access_token: token)
          end
        end

    end
  end
end