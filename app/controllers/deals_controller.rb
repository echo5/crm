class DealsController < ApplicationController
  before_action :set_deal, only: [:show, :edit, :update, :destroy]

  # GET /deals
  # GET /deals.json
  def index
    @stages = Stage.all
    @deals = Deal.all
  end

  # GET /deals/1
  # GET /deals/1.json
  def show
  end

  # GET /deals/new
  def new
    @deal = Deal.new
    render partial: 'new'
  end

  # GET /deals/1/edit
  def edit
    render partial: 'edit'
  end

  # POST /deals
  # POST /deals.json
  def create
    @deal = Deal.new(deal_params)
    if @deal.save
      flash[:success] = 'Deal was successfully created.'
      render :js => "Turbolinks.visit('#{deals_url}')"      
    else
      flash[:error] = @deal.errors.full_messages.join('<br>')
    end
  end

  # PATCH/PUT /deals/1
  # PATCH/PUT /deals/1.json
  def update
    if @deal.update(deal_params)
      flash[:success] = 'Deal was successfully updated.'
      render :js => "Turbolinks.visit('#{deals_url}')"      
    else
      flash[:error] = @deal.errors.full_messages.join('<br>')
    end
  end

  # DELETE /deals/1
  # DELETE /deals/1.json
  def destroy
    @deal.destroy
    flash[:success] = 'Deal was successfully destroyed.'
    redirect_to deals_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_deal
      @deal = Deal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def deal_params
      params.require(:deal).permit(:status, :name, :stage_id, :value, :close_date, :contact_id, :owner_id)
    end
end
