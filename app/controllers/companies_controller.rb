class CompaniesController < ApplicationController
  before_action :set_company, only: [:show, :edit, :update, :destroy]

  # GET /companies
  # GET /companies.json
  def index
    @companies = Company.all.page params[:page]
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
  end

  # GET /companies/new
  def new
    @company = Company.new
    render partial: 'new'    
  end

  # GET /companies/1/edit
  def edit
    render partial: 'edit'    
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(company_params)
    if @company.save
      flash[:success] = 'Company was successfully created.'
      render :js => "Turbolinks.visit('#{company_url(@company)}')"      
    else
      flash[:error] = @company.errors.full_messages.join('<br>')
    end
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    if @company.update(company_params)
      flash[:success] = 'Company was successfully updated.'
      render :js => "Turbolinks.visit('#{company_url(@company)}')"              
    else
      flash[:error] = @company.errors.full_messages.join('<br>')
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company.destroy
    flash[:success] = 'Company was successfully destroyed.'
    redirect_to companies_url
  end

  # GET /companies/import
  def import
    import = Company.import_csv(params[:companies_import_file])
    if import[:total_imported] > 0
      flash[:success] = pluralize(import[:total_imported], 'companies') + ' were successfully imported.'
    end
    if import[:errors]
      flash[:error] = import[:errors]
    end
    redirect_to companies_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:name, :website, :domain, :address1, :address2, :city, :state, :zip, :country, :industry, :time_zone, :description)
    end
end
