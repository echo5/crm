class ContactSequencesController < ApplicationController
  before_action :set_contact_sequence, only: [:show, :edit, :update, :destroy]

  # GET /contact_sequences/new
  def new
    @contact_sequence = ContactSequence.new(contact_id: params[:contact_id])
    render partial: 'new'
  end

  # POST /contact_sequences
  # POST /contact_sequences.json
  def create
    @contact_sequence = ContactSequence.new(contact_sequence_params)

    if @contact_sequence.save
      redirect_to @contact_sequence, notice: 'Contact was added to sequence successfully.'
    else
      flash[:error] = @contact_sequence.errors.full_messages.join('<br>')
    end
  end

  # DELETE /contact_sequences/1
  # DELETE /contact_sequences/1.json
  def destroy
    @contact_sequence.destroy
    redirect_to :back, notice: 'Contact was removed from sequence.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact_sequence
      @contact_sequence = ContactSequence.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_sequence_params
      params.require(:contact_sequence).permit(:step_id, :contact_id, :sequence_id, :status)
    end
end
