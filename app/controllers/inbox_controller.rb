class InboxController < ApplicationController

  # GET /inbox
  def index
    # where(contacts: {owner_id: 1})
    @emails = Email.includes(:contact).where(user_id: nil).filter(params.slice(:subject, :owner)).page params[:page]
  end

end
