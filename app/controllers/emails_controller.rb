class EmailsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:opt_in, :opt_out]
  before_action :set_email, only: [:click, :open, :opt_out, :opt_in]

  def index
    @emails = Email.includes(:contact).where(user_id: nil).filter(params.slice(:subject, :owner)).page params[:page]    
  end

  def send_email
    @email = Email.new(email_params)
    @email.user_id = current_user.id
    @email.track_open = true
    @email.track_clicks = true
    @email.from = @email.email_account.from
    mailer = ContactMailer.with(
      email: @email
    ).default.deliver
    @email.message_id = mailer.message_id
    if @email.save
      flash[:success] = 'Email sent'
    else
      render json: @email.errors, status: :unprocessable_entity
    end
  end

  def open
    if @email
      @email.opens.build(created_at: Time.now)
      @email.save!
    end
    send_data Base64.decode64("R0lGODlhAQABAPAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="), type: "image/gif", disposition: "inline"
  end

  def click
    url = params[:url].to_s
    signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new("sha1"), Rails.application.secrets.secret_key_base, url)
    if secure_compare(params[:signature].to_s, signature.to_s)
      if @email
        @link_click = LinkClick.new(contact_id: @email.contact_id, url: url)
        @link_click.save!
      end
      redirect_to url
    else
      redirect_to main_app.root_url
    end
  end

  def opt_out
    @email.contact.dnc_reason = 'opted_out'
    @email.contact.save!
  end

  def opt_in
    @email.contact.dnc_reason = nil
    @email.contact.save!
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_email
      @email = Email.where(token: params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def email_params
      params.require(:email).permit(:contact_id, :email_account_id, :to, :subject, :body, :in_reply_to)
    end
    
    # from https://github.com/rails/rails/blob/master/activesupport/lib/active_support/message_verifier.rb
    # constant-time comparison algorithm to prevent timing attacks
    def secure_compare(a, b)
      return false unless a.bytesize == b.bytesize

      l = a.unpack "C#{a.bytesize}"

      res = 0
      b.each_byte { |byte| res |= byte ^ l.shift }
      res == 0
    end

end
