class SequencesController < ApplicationController
  before_action :set_sequence, only: [:show, :edit, :update, :destroy]

  # GET /sequences
  # GET /sequences.json
  def index
    @sequences = Sequence.all.page params[:page]
  end

  # GET /sequences/1
  # GET /sequences/1.json
  def show
    @sequence_step = SequenceStep.new
  end

  # GET /sequences/new
  def new
    @sequence = Sequence.new
    Hour.days.each do |day, index|
      @sequence.hours.build(day: day)
    end
    render partial: 'new'
  end

  def add_step
    @sequence = Sequence.new
    @sequence.steps.build
    render "add_tree", layout: false
  end

  # GET /sequences/1/edit
  def edit
    render partial: 'edit'
  end

  # POST /sequences
  # POST /sequences.json
  def create
    @sequence = Sequence.new(sequence_params)
    if @sequence.save
      flash[:success] = 'Sequence was successfully created.'
      redirect_to @sequence
    else
      flash[:error] = @sequence.errors.full_messages.join("<br>")
    end
  end

  # PATCH/PUT /sequences/1
  # PATCH/PUT /sequences/1.json
  def update
    if @sequence.update(sequence_params)
      redirect_to @sequence, notice: 'Sequence was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /sequences/1
  # DELETE /sequences/1.json
  def destroy
    @sequence.destroy
    flash[:success] = 'Sequence was successfully destroyed.'
    redirect_to sequences_url
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sequence
      @sequence = Sequence.includes(:steps, :hours).find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sequence_params
      params.require(:sequence).permit(:name, :active, :email_account_id, :time_zone, segment_ids: [], hours_attributes: [:id, :day, :open, :start, :end], steps_attributes: [:condition, :action, :triggered_by])
    end
end
