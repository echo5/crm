$(document).on('ready', function() {

  $( "#q" ).keyup(function() {
    if ($(this).val().length >= 3) {
      $('#search-overlay').show();      
      $('#search-results').show();      
      $(this).submit();
    }
  });

  $('#search-overlay').click(function(){
    $('#search-results').hide();
    $(this).hide();
  }); 

});