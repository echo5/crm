$(document).on('click', '.reply-to', function(e) {
  e.preventDefault();
  $('#email_subject').val('Re:' + $(this).data('subject')).attr('readonly', true);
  $('#email_in_reply_to').val($(this).data('message-id'));
  $('.cancel-reply-to').show();
});

$(document).on('click', '.cancel-reply-to', function(e) {
  e.preventDefault();
  $('#email_subject').val('').removeAttr('readonly');
  $('#email_in_reply_to').val('');
  $('.cancel-reply-to').hide();
});