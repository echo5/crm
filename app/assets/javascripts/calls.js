// Store some selectors for elements we'll reuse
var callStatus = null;
var answerButton = null;
var callSupportButton = null;
var hangUpButton = null;
var callCustomerButtons = null;
var activeConnection = null;

/* Helper function to update the call status bar */
function updateCallStatus(status) {
  callStatus.html(status);
}

/* Get a Twilio Client token with an AJAX request */
$(document).on('ready', function() {

  // Store some selectors for elements we'll reuse
  callerName = $("#caller-name");
  callStatus = $("#call-status");
  answerButton = $(".answer-button");
  callSupportButton = $(".call-support-button");
  hangUpButton = $(".hangup-button");
  callCustomerButtons = $(".call-customer-button");


  $('#phone').click(function(e) {
    e.preventDefault();
    $('#phone-controls').toggle();
  });

  $('#dial').click(function(e) {
    callNumber($('#phone-input').val());
  });
  
  $.post("/calls/generate_token", {page: window.location.pathname}, function(data) {
    Twilio.Device.setup(data.token);
  });
});

/* Callback to let us know Twilio Client is ready */
Twilio.Device.ready(function (device) {
  updateCallStatus("Ready");
});

/* Call a customer from a support ticket */
function callNumber(phoneNumber) {
  $('body').addClass('call-active');
  updateCallStatus("Calling " + phoneNumber + "...");
  var params = {"phoneNumber": phoneNumber};
  Twilio.Device.connect(params);
}

/* Callback for when Twilio Client initiates a new connection */
Twilio.Device.connect(function (connection) {
  activeConnection = connection;
  if ("phoneNumber" in connection.message) {
    updateCallStatus("In call with " + connection.message.phoneNumber);
  }

  connection.disconnect(function() {
    $('body').removeClass('call-incoming').removeClass('call-active');  
    updateCallStatus("Ready");
  });
});

/* End a call */
function hangUp() {
  Twilio.Device.disconnectAll();
  updateCallStatus("Call ended");  
  $('body').removeClass('call-active');
}

Twilio.Device.incoming(function(connection) {
  $.post("/calls/get_caller", {from: connection.parameters.From}, function(data) {
    if (data.name) {
      callerName.text(data.name);
    } else {
      callerName.text('');
    }
  });
  $('body').addClass('call-incoming');  
  updateCallStatus("Incoming call from " + connection.parameters.From);  
  $('.answer-button').click(function() {
    connection.accept();
    $('body').removeClass('call-incoming').addClass('call-active');
  });
});

Twilio.Device.cancel(function (connection) {
  $('body').removeClass('call-incoming').removeClass('call-active');  
  updateCallStatus("Ready");
});