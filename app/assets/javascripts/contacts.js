$(document).on('ready page:load', function() {
  $('[data-toggle="tooltip"]').tooltip();

  $('.select2').each(function() {
    var options = $(this).data('options');
    $(this).select2(options).on("change", function(e) {
      var isNew = $(this).find('[data-select2-tag="true"]');
      if(isNew.length){
        isNew.replaceWith('<option selected value="'+isNew.val()+'">'+isNew.val()+'</option>');
        $.ajax({
          url: '/contact_tags',
          type: 'POST',
          data: {'contact_tag[name]' : isNew.val()},
          dataType: 'json',
          success: function(data) {
          }
        });
      }
    });;
  });
})