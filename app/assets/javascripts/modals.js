$(document).on('ajax:success', '.modal-trigger', function(event, data, status, xhr) {
  createModal(data.content);
});

// $(document).on('click', '.modal-trigger', function(event) {
//   event.preventDefault();
//   var url = $(this).attr('href');
//   $.ajax( {
//     url: url, 
//     type: 'get',
//     dataType: 'json',
//     success: function( data ) {
//       createModal(data.content);
//     }
//   });
// });

function createModal(content) {
  var modal = $('#ajax-modal');
  if (!modal.length) {
    var html = `<div id="ajax-modal" class="modal fade ajax-modal" tabindex="-1" role="dialog" aria-hidden="true">\n\
    <div class="modal-dialog modal-lg">\n\
    <div class="modal-content">\n\
    <div class="modal-body">\n\
    ...\n\
    </div>\n\
    </div>\n\
    </div>\n\
    </div>`;
    modal = $(html).appendTo('body');
  }
  modal.find('.modal-body').html(content);
  modal.modal('show');
}
  