$(document).on('change', '#sequence_step_action', function() {
  var selected = $(this).val();
  $('div[data-action]').hide();
  $('[data-action=' + selected + ']').show();
});