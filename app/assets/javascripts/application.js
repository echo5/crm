// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbograft
//= require cocoon
//= require popper
//= require tether
//= require bootstrap
//= require select2
//= require modals
//= require contacts
//= require workflow
//= require search
//= require sequence_steps
//= require emails
//= require calls
//= require dagre
//= require jsplumb.min
//= require trix

$(document).on('ready', function() {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
});

$(document).on('ready page:load', function() {
  $('#nav-side a').each(function() {
    $(this).removeClass('active');
    if ($(this).attr('href') == window.location.href || $(this).attr('href') == window.location.pathname) {
      $(this).addClass('active');
    }
  });
});

$(document).on('change', 'form.auto-submit, .auto-submit select, .auto-submit input', function() {
  $(this).trigger('submit.rails');
});


var show_ajax_message = function(msg, type) {
  $("#notifications").html('<div class="alert alert-'+type+'">'+msg+'</div>');
};

$(document).ajaxComplete(function(event, request) {
  console.log(request.getResponseHeader('X-Message'));
  var msg = request.getResponseHeader('X-Message');
  var type = request.getResponseHeader('X-Message-Type');
  show_ajax_message(msg, type);
});