$(document).on('click', '.workflow-add-step', function() {
  var href = $(this).siblings('.modal-trigger').attr('href');
  var link = document.createElement('a');
  link.dataset.remote = "true";
  link.classList = 'modal-trigger';
  link.href = href;
  $(link).appendTo('body');
  link.click();
})

$(document).on('ajax:success', '#new_sequence_step, .edit_sequence_step', function(event, data, status, xhr) {
  Turbolinks.visit(window.location);
  // var childStep = $(data);
  // childStep.attr('id')
});