class ContactMailer < ApplicationMailer
  attr_accessor :email
  before_action :set_email_account_and_contact,
                :parse_template

  after_action :set_delivery_options,
               :prevent_delivery_to_dnc,
               :set_reply_to_headers,
               :set_open_tracking,
               :set_click_tracking

  def default
    mail(
      from: @email.email_account.from,
      to: @email.contact.email,
      body: @body,
      content_type: "text/html",
      subject: @email.subject,
    )
  end

  private

    def set_email_account_and_contact
      @email = params[:email]
      @email.to = @email.contact.email
    end

    def parse_template
      template = Liquid::Template.parse(@email.body)
      output = template.render('contact' => @email.contact.attributes, :error_mode => :strict)
      @email.body = output
      @body = @email.body.dup
    end

    def set_delivery_options
      if @email.email_account
        delivery_options = { 
          user_name: @email.email_account.smtp_username,
          password: @email.email_account.smtp_password,
          address: @email.email_account.smtp_host,
          port: @email.email_account.smtp_port.to_i,
          authentication: 'plain',
          enable_starttls_auto: true
        }
        mail.delivery_method.settings.merge!(delivery_options)
      end
    end

    def prevent_delivery_to_dnc
      if @email.contact.dnc_reason
        mail.perform_deliveries = false
      end
    end

    def set_reply_to_headers
      if !@email.in_reply_to.blank?
        headers["In-Reply-To"] = "<#{@email.in_reply_to}>"
        headers["References"] = "<#{@email.in_reply_to}>"
      end
    end

    def set_open_tracking
      if @email.track_open && html_part?
        raw_source = (message.html_part || message).body.raw_source
        regex = /<\/body>/i
        url =
          url_for(
            controller: "emails",
            action: "open",
            id: @email.token,
            format: "gif"
          )
        pixel = ActionController::Base.helpers.image_tag(url, size: "1x1", alt: "")

        # try to add before body tag
        if raw_source.match(regex)
          raw_source.gsub!(regex, "#{pixel}\\0")
        else
          raw_source << pixel
        end
      end
    end

    def set_click_tracking
      if @email.track_clicks && html_part?
        body = (message.html_part || message).body
        doc = Nokogiri::HTML(body.raw_source)
        doc.css("a[href]").each do |link|
          uri = parse_uri(link["href"])
          next unless trackable?(uri)

          if !skip_attribute?(link, "click")
            signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new("sha1"), Rails.application.secrets.secret_key_base, link["href"])
            link["href"] =
              url_for(
                controller: "emails",
                action: "click",
                id: @email.token,
                url: link["href"],
                signature: signature
              )
          end
        end

        # hacky
        body.raw_source.sub!(body.raw_source, doc.to_s)
      end
    end

    def html_part?
      (message.html_part || message).content_type =~ /html/
    end

    def skip_attribute?(link, suffix)
      attribute = "data-skip-#{suffix}"
      if link[attribute]
        # remove it
        link.remove_attribute(attribute)
        true
      elsif link["href"].to_s =~ /unsubscribe/i && !options[:unsubscribe_links]
        # try to avoid unsubscribe links
        true
      else
        false
      end
    end

    # Filter trackable URIs, i.e. absolute one with http
    def trackable?(uri)
      uri && uri.absolute? && %w(http https).include?(uri.scheme)
    end

    # Parse href attribute
    # Return uri if valid, nil otherwise
    def parse_uri(href)
      # to_s prevent to return nil from this method
      Addressable::URI.parse(href.to_s) rescue nil
    end


    

end
