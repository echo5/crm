json.extract! stage, :id, :name, :win_probability, :created_at, :updated_at
json.url stage_url(stage, format: :json)
