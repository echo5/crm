json.extract! company, :id, :name, :website, :domain, :address1, :address2, :city, :state, :zip, :country, :industry, :time_zone, :description, :created_at, :updated_at
json.url company_url(company, format: :json)
