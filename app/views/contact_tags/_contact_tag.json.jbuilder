json.extract! contact_tag, :id, :name, :created_at, :updated_at
json.url contact_tag_url(contact_tag, format: :json)
