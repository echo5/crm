json.extract! sequence, :id, :name, :status, :from_id, :time_zone, :delivery_times, :created_at, :updated_at
json.url sequence_url(sequence, format: :json)
