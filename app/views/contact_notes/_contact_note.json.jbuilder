json.extract! contact_note, :id, :contact_id, :content, :created_at, :updated_at
json.url contact_note_url(contact_note, format: :json)
