json.extract! email, :id, :contact_id, :from, :to, :header, :subject, :body, :replied, :opened, :created_at, :updated_at
json.url email_url(email, format: :json)
