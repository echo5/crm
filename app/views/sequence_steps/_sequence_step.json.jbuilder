json.extract! sequence_step, :id, :sequence_id, :triggered_by_id, :condition, :action, :object, :created_at, :updated_at
json.url sequence_step_url(sequence_step, format: :json)
