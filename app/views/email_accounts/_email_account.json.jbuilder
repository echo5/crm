json.extract! email_account, :id, :imap_host, :imap_port, :imap_encryption, :imap_username, :imap_password, :smtp_host, :smtp_port, :smtp_encryption, :smtp_username, :smtp_password, :created_at, :updated_at
json.url email_account_url(email_account, format: :json)
