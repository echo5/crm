json.extract! sequence_contact, :id, :step_id, :contact_id, :sequence_id, :status, :created_at, :updated_at
json.url sequence_contact_url(sequence_contact, format: :json)
