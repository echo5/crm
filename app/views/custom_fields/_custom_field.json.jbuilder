json.extract! custom_field, :id, :label, :name, :model, :field_type, :required, :created_at, :updated_at
json.url custom_field_url(custom_field, format: :json)
