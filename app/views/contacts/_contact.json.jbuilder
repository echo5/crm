json.extract! contact, :id, :title, :first_name, :last_name, :email, :company_id, :position, :address1, :address2, :city, :state, :zip, :country, :mobile_phone, :work_phone, :website, :stage_id, :industry, :source, :owner_id, :created_at, :updated_at
json.url contact_url(contact, format: :json)
