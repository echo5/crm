json.extract! deal, :id, :status, :name, :stage_id, :value, :close_date, :contact_id, :owner_id, :created_at, :updated_at
json.url deal_url(deal, format: :json)
