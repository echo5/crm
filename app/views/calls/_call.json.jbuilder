json.extract! call, :id, :contact_id, :to, :from, :caller_name, :duration, :status, :direction, :created_at, :updated_at
json.url call_url(call, format: :json)
