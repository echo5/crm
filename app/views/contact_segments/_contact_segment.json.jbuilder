json.extract! contact_segment, :id, :name, :description, :created_at, :updated_at
json.url contact_segment_url(contact_segment, format: :json)
