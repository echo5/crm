class ToggleInput < SimpleForm::Inputs::FileInput
  def input(wrapper_options = nil)
    template.content_tag(:div, class: 'switch-wrapper') do
    template.content_tag(:div, class: 'switch') do
      template.concat @builder.check_box(attribute_name, input_html_options)
      template.concat @builder.label(attribute_name, switch_group)
    end
    end
  end

  def switch_group
    '<span class="switch-inner"><span class="switch-off">Off</span><span class="switch-on">On</span></span><span class="switch-btn"></span>'.html_safe
  end

end