# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180516153032) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "api_keys", force: :cascade do |t|
    t.string "name"
    t.string "access_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "calls", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "contact_id"
    t.string "to"
    t.string "from"
    t.string "caller_name"
    t.integer "duration"
    t.integer "status"
    t.integer "direction"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_calls_on_contact_id"
    t.index ["user_id"], name: "index_calls_on_user_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "website"
    t.string "domain"
    t.string "address1"
    t.string "address2"
    t.string "city"
    t.string "state"
    t.string "zip"
    t.string "country", limit: 2
    t.integer "industry"
    t.string "time_zone"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "conditions", force: :cascade do |t|
    t.string "conditionable_type"
    t.bigint "conditionable_id"
    t.integer "logical_operator"
    t.string "property"
    t.integer "comparison_operator"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["conditionable_type", "conditionable_id"], name: "index_conditions_on_conditionable_type_and_conditionable_id"
  end

  create_table "contact_notes", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "contact_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_contact_notes_on_contact_id"
    t.index ["user_id"], name: "index_contact_notes_on_user_id"
  end

  create_table "contact_segments", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contact_segments_contacts", id: false, force: :cascade do |t|
    t.bigint "contact_segment_id", null: false
    t.bigint "contact_id", null: false
    t.index ["contact_segment_id", "contact_id"], name: "index_contact_segment_id_contact_id"
  end

  create_table "contact_segments_sequences", id: false, force: :cascade do |t|
    t.bigint "sequence_id", null: false
    t.bigint "contact_segment_id", null: false
    t.index ["sequence_id", "contact_segment_id"], name: "index_sequence_id_contact_segment_id"
  end

  create_table "contact_sequences", force: :cascade do |t|
    t.bigint "contact_id"
    t.bigint "sequence_id"
    t.bigint "sequence_step_id"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_contact_sequences_on_contact_id"
    t.index ["sequence_id"], name: "index_contact_sequences_on_sequence_id"
    t.index ["sequence_step_id"], name: "index_contact_sequences_on_sequence_step_id"
  end

  create_table "contact_tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contact_tags_contacts", id: false, force: :cascade do |t|
    t.bigint "contact_tag_id", null: false
    t.bigint "contact_id", null: false
    t.index ["contact_tag_id", "contact_id"], name: "index_contact_tags_contacts_on_contact_tag_id_and_contact_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "title"
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.bigint "company_id"
    t.string "job_title"
    t.string "address1"
    t.string "address2"
    t.string "city"
    t.string "state"
    t.string "zip"
    t.string "country", limit: 2
    t.string "mobile_phone"
    t.string "work_phone"
    t.string "website"
    t.integer "lifecycle_stage"
    t.integer "industry"
    t.integer "dnc_reason"
    t.string "source"
    t.string "ip_address"
    t.bigint "owner_id"
    t.datetime "last_activity_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["company_id"], name: "index_contacts_on_company_id"
    t.index ["owner_id"], name: "index_contacts_on_owner_id"
  end

  create_table "custom_fields", force: :cascade do |t|
    t.string "label"
    t.string "name"
    t.integer "model"
    t.integer "field_type"
    t.text "options"
    t.boolean "required"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deals", force: :cascade do |t|
    t.integer "status"
    t.string "name"
    t.bigint "stage_id"
    t.decimal "value"
    t.datetime "close_date"
    t.bigint "contact_id"
    t.bigint "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_deals_on_contact_id"
    t.index ["owner_id"], name: "index_deals_on_owner_id"
    t.index ["stage_id"], name: "index_deals_on_stage_id"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "email_accounts", force: :cascade do |t|
    t.string "from"
    t.string "imap_host"
    t.integer "imap_port"
    t.string "imap_username"
    t.string "imap_password"
    t.string "smtp_host"
    t.integer "smtp_port"
    t.string "smtp_username"
    t.string "smtp_password"
    t.integer "last_uid_checked"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "email_opens", force: :cascade do |t|
    t.bigint "email_id"
    t.bigint "contact_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_email_opens_on_contact_id"
    t.index ["email_id"], name: "index_email_opens_on_email_id"
  end

  create_table "email_templates", force: :cascade do |t|
    t.string "name"
    t.string "subject"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "emails", force: :cascade do |t|
    t.bigint "contact_id"
    t.bigint "user_id"
    t.bigint "email_account_id"
    t.string "from"
    t.string "to"
    t.datetime "date"
    t.string "subject"
    t.text "body"
    t.string "token"
    t.string "message_id"
    t.string "in_reply_to"
    t.boolean "replied", default: false
    t.bigint "email_template_id"
    t.bigint "sequence_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_emails_on_contact_id"
    t.index ["email_account_id"], name: "index_emails_on_email_account_id"
    t.index ["email_template_id"], name: "index_emails_on_email_template_id"
    t.index ["sequence_id"], name: "index_emails_on_sequence_id"
    t.index ["user_id"], name: "index_emails_on_user_id"
  end

  create_table "hours", force: :cascade do |t|
    t.string "hourable_type"
    t.bigint "hourable_id"
    t.boolean "open"
    t.integer "day"
    t.time "start"
    t.time "end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hourable_type", "hourable_id"], name: "index_hours_on_hourable_type_and_hourable_id"
  end

  create_table "link_clicks", force: :cascade do |t|
    t.bigint "contact_id"
    t.text "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_link_clicks_on_contact_id"
  end

  create_table "sequence_steps", force: :cascade do |t|
    t.bigint "sequence_id"
    t.bigint "trigger_id"
    t.integer "action"
    t.integer "path", default: 0
    t.jsonb "properties"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sequence_id"], name: "index_sequence_steps_on_sequence_id"
    t.index ["trigger_id"], name: "index_sequence_steps_on_trigger_id"
  end

  create_table "sequences", force: :cascade do |t|
    t.string "name"
    t.boolean "active", default: false
    t.boolean "end_on_reply"
    t.bigint "email_account_id"
    t.string "time_zone"
    t.jsonb "delivery_times"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email_account_id"], name: "index_sequences_on_email_account_id"
  end

  create_table "stages", force: :cascade do |t|
    t.string "name"
    t.integer "win_probability"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.integer "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "calls", "contacts"
  add_foreign_key "calls", "users"
  add_foreign_key "contact_notes", "contacts"
  add_foreign_key "contact_notes", "users"
  add_foreign_key "contact_sequences", "contacts"
  add_foreign_key "contact_sequences", "sequence_steps"
  add_foreign_key "contact_sequences", "sequences"
  add_foreign_key "contacts", "companies"
  add_foreign_key "deals", "contacts"
  add_foreign_key "deals", "stages"
  add_foreign_key "email_opens", "contacts"
  add_foreign_key "email_opens", "emails"
  add_foreign_key "emails", "contacts"
  add_foreign_key "link_clicks", "contacts"
  add_foreign_key "sequence_steps", "sequences"
end
