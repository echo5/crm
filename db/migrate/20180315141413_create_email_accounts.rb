class CreateEmailAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :email_accounts do |t|
      t.string :from
      t.string :imap_host
      t.integer :imap_port
      t.string :imap_username
      t.string :imap_password
      t.string :smtp_host
      t.integer :smtp_port
      t.string :smtp_username
      t.string :smtp_password
      t.integer :last_uid_checked

      t.timestamps
    end
  end
end
