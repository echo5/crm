class CreateSequences < ActiveRecord::Migration[5.1]
  def change
    create_table :sequences do |t|
      t.string :name
      t.boolean :active, default: false
      t.boolean :end_on_reply
      t.references :email_account
      t.string :time_zone
      t.jsonb :delivery_times

      t.timestamps
    end
  end
end
