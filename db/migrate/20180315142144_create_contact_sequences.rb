class CreateContactSequences < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_sequences do |t|
      t.references :contact, foreign_key: true
      t.references :sequence, foreign_key: true
      t.references :sequence_step, foreign_key: true
      t.integer :status

      t.timestamps
    end
  end
end
