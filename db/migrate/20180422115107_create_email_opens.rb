class CreateEmailOpens < ActiveRecord::Migration[5.1]
  def change
    create_table :email_opens do |t|
      t.references :email, foreign_key: true
      t.references :contact, foreign_key: true

      t.timestamps
    end
  end
end
