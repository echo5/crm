class CreateJoinTableContactTagsContacts < ActiveRecord::Migration[5.1]
  def change
    create_join_table :contact_tags, :contacts do |t|
      t.index [:contact_tag_id, :contact_id]
      # t.index [:contact_id, :contact_tag_id]
    end
  end
end
