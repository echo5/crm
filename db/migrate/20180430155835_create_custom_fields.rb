class CreateCustomFields < ActiveRecord::Migration[5.1]
  def change
    create_table :custom_fields do |t|
      t.string :label
      t.string :name
      t.integer :model
      t.integer :field_type
      t.text :options
      t.boolean :required

      t.timestamps
    end
  end
end
