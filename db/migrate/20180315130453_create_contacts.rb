class CreateContacts < ActiveRecord::Migration[5.1]
  def change
    create_table :contacts do |t|
      t.string :title
      t.string :first_name
      t.string :last_name
      t.string :email
      t.references :company, foreign_key: true
      t.string :job_title
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip
      t.string :country, limit: 2
      t.string :mobile_phone
      t.string :work_phone
      t.string :website
      t.integer :lifecycle_stage      
      t.integer :industry
      t.integer :dnc_reason
      t.string :source
      t.string :ip_address
      t.references :owner, references: :users
      t.datetime :last_activity_at

      t.timestamps
    end
  end
end
