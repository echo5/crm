class CreateContactNotes < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_notes do |t|
      t.references :user, foreign_key: true
      t.references :contact, foreign_key: true
      t.text :content

      t.timestamps
    end
  end
end
