class CreateSequenceSteps < ActiveRecord::Migration[5.1]
  def change
    create_table :sequence_steps do |t|
      t.references :sequence, foreign_key: true
      t.references :trigger, references: :sequence_steps
      t.integer :action
      t.integer :path, default: 0
      t.jsonb :properties

      t.timestamps
    end
  end
end
