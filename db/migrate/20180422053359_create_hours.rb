class CreateHours < ActiveRecord::Migration[5.1]
  def change
    create_table :hours do |t|
      t.references :hourable, polymorphic: true
      t.boolean :open
      t.integer :day
      t.time :start
      t.time :end

      t.timestamps
    end
  end
end
