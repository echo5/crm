class CreateJoinTableContactSegmentsContacts < ActiveRecord::Migration[5.1]
  def change
    create_join_table :contact_segments, :contacts do |t|
      t.index [:contact_segment_id, :contact_id], name: 'index_contact_segment_id_contact_id'
      # t.index [:contact_id, :contact_segment_id]
    end
  end
end
