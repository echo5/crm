class CreateContactSegments < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_segments do |t|
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
