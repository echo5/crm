class CreateCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :website
      t.string :domain
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :zip
      t.string :country, limit: 2
      t.integer :industry
      t.string :time_zone
      t.text :description

      t.timestamps
    end
  end
end
