class CreateJoinTableSequencesContactSegments < ActiveRecord::Migration[5.1]
  def change
    create_join_table :sequences, :contact_segments do |t|
      t.index [:sequence_id, :contact_segment_id], name: 'index_sequence_id_contact_segment_id'
      # t.index [:contact_segment_id, :sequence_id]
    end
  end
end
