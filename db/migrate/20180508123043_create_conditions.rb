class CreateConditions < ActiveRecord::Migration[5.1]
  def change
    create_table :conditions do |t|
      t.references :conditionable, polymorphic: true
      t.integer :logical_operator
      t.string :property
      t.integer :comparison_operator
      t.string :value

      t.timestamps
    end
  end
end
