class CreateLinkClicks < ActiveRecord::Migration[5.1]
  def change
    create_table :link_clicks do |t|
      t.references :contact, foreign_key: true
      t.text :url

      t.timestamps
    end
  end
end
