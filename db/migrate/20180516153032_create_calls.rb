class CreateCalls < ActiveRecord::Migration[5.1]
  def change
    create_table :calls do |t|
      t.references :user, foreign_key: true
      t.references :contact, foreign_key: true
      t.string :to
      t.string :from
      t.string :caller_name
      t.integer :duration
      t.integer :status
      t.integer :direction

      t.timestamps
    end
  end
end
