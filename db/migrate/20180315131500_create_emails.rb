class CreateEmails < ActiveRecord::Migration[5.1]
  def change
    create_table :emails do |t|
      t.references :contact, foreign_key: true
      t.references :user
      t.references :email_account
      t.string :from
      t.string :to
      t.datetime :date
      t.string :subject
      t.text :body
      t.string :token
      t.string :message_id
      t.string :in_reply_to
      t.boolean :replied, default: false
      t.references :email_template
      t.references :sequence

      t.timestamps
    end
  end
end
