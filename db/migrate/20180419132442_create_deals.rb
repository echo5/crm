class CreateDeals < ActiveRecord::Migration[5.1]
  def change
    create_table :deals do |t|
      t.integer :status
      t.string :name
      t.references :stage, foreign_key: true
      t.decimal :value
      t.datetime :close_date
      t.references :contact, foreign_key: true
      t.references :owner, references: :contacts

      t.timestamps
    end
  end
end
