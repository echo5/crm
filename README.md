# CRM

CRM for managing contacts, vendors, and workflow automation. Logs incoming emails and calls associated with contacts as well as pipeline management.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Ruby 2.3.0
* Postsgres 10+

### Installing

Run bundler like any other Rails app.

```
bundle install
```

Create a dotenv file for with the relevant information

```
SECRET_KEY_BASE=
TWILIO_ACCOUNT_SID=
TWILIO_AUTH_TOKEN=
TWILIO_NUMBER=
TWIML_APPLICATION_SID=
```

## Running the tests

Tests can be run with:

```
bundle exec rake test
bundle exec rake spec
```