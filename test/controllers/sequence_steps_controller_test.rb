require 'test_helper'

class SequenceStepsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sequence_step = sequence_steps(:one)
  end

  test "should get index" do
    get sequence_steps_url
    assert_response :success
  end

  test "should get new" do
    get new_sequence_step_url
    assert_response :success
  end

  test "should create sequence_step" do
    assert_difference('SequenceStep.count') do
      post sequence_steps_url, params: { sequence_step: { action: @sequence_step.action, condition: @sequence_step.condition, object: @sequence_step.object, sequence_id: @sequence_step.sequence_id, triggered_by_id: @sequence_step.triggered_by_id } }
    end

    assert_redirected_to sequence_step_url(SequenceStep.last)
  end

  test "should show sequence_step" do
    get sequence_step_url(@sequence_step)
    assert_response :success
  end

  test "should get edit" do
    get edit_sequence_step_url(@sequence_step)
    assert_response :success
  end

  test "should update sequence_step" do
    patch sequence_step_url(@sequence_step), params: { sequence_step: { action: @sequence_step.action, condition: @sequence_step.condition, object: @sequence_step.object, sequence_id: @sequence_step.sequence_id, triggered_by_id: @sequence_step.triggered_by_id } }
    assert_redirected_to sequence_step_url(@sequence_step)
  end

  test "should destroy sequence_step" do
    assert_difference('SequenceStep.count', -1) do
      delete sequence_step_url(@sequence_step)
    end

    assert_redirected_to sequence_steps_url
  end
end
