require 'test_helper'

class ContactSegmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contact_segment = contact_segments(:one)
  end

  test "should get index" do
    get contact_segments_url
    assert_response :success
  end

  test "should get new" do
    get new_contact_segment_url
    assert_response :success
  end

  test "should create contact_segment" do
    assert_difference('ContactSegment.count') do
      post contact_segments_url, params: { contact_segment: { description: @contact_segment.description, name: @contact_segment.name } }
    end

    assert_redirected_to contact_segment_url(ContactSegment.last)
  end

  test "should show contact_segment" do
    get contact_segment_url(@contact_segment)
    assert_response :success
  end

  test "should get edit" do
    get edit_contact_segment_url(@contact_segment)
    assert_response :success
  end

  test "should update contact_segment" do
    patch contact_segment_url(@contact_segment), params: { contact_segment: { description: @contact_segment.description, name: @contact_segment.name } }
    assert_redirected_to contact_segment_url(@contact_segment)
  end

  test "should destroy contact_segment" do
    assert_difference('ContactSegment.count', -1) do
      delete contact_segment_url(@contact_segment)
    end

    assert_redirected_to contact_segments_url
  end
end
