require 'test_helper'

class SequenceContactsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sequence_contact = sequence_contacts(:one)
  end

  test "should get index" do
    get sequence_contacts_url
    assert_response :success
  end

  test "should get new" do
    get new_sequence_contact_url
    assert_response :success
  end

  test "should create sequence_contact" do
    assert_difference('SequenceContact.count') do
      post sequence_contacts_url, params: { sequence_contact: { contact_id: @sequence_contact.contact_id, sequence_id: @sequence_contact.sequence_id, status: @sequence_contact.status, step_id: @sequence_contact.step_id } }
    end

    assert_redirected_to sequence_contact_url(SequenceContact.last)
  end

  test "should show sequence_contact" do
    get sequence_contact_url(@sequence_contact)
    assert_response :success
  end

  test "should get edit" do
    get edit_sequence_contact_url(@sequence_contact)
    assert_response :success
  end

  test "should update sequence_contact" do
    patch sequence_contact_url(@sequence_contact), params: { sequence_contact: { contact_id: @sequence_contact.contact_id, sequence_id: @sequence_contact.sequence_id, status: @sequence_contact.status, step_id: @sequence_contact.step_id } }
    assert_redirected_to sequence_contact_url(@sequence_contact)
  end

  test "should destroy sequence_contact" do
    assert_difference('SequenceContact.count', -1) do
      delete sequence_contact_url(@sequence_contact)
    end

    assert_redirected_to sequence_contacts_url
  end
end
