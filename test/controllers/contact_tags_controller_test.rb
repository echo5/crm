require 'test_helper'

class ContactTagsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contact_tag = contact_tags(:one)
  end

  test "should get index" do
    get contact_tags_url
    assert_response :success
  end

  test "should get new" do
    get new_contact_tag_url
    assert_response :success
  end

  test "should create contact_tag" do
    assert_difference('ContactTag.count') do
      post contact_tags_url, params: { contact_tag: { name: @contact_tag.name } }
    end

    assert_redirected_to contact_tag_url(ContactTag.last)
  end

  test "should show contact_tag" do
    get contact_tag_url(@contact_tag)
    assert_response :success
  end

  test "should get edit" do
    get edit_contact_tag_url(@contact_tag)
    assert_response :success
  end

  test "should update contact_tag" do
    patch contact_tag_url(@contact_tag), params: { contact_tag: { name: @contact_tag.name } }
    assert_redirected_to contact_tag_url(@contact_tag)
  end

  test "should destroy contact_tag" do
    assert_difference('ContactTag.count', -1) do
      delete contact_tag_url(@contact_tag)
    end

    assert_redirected_to contact_tags_url
  end
end
