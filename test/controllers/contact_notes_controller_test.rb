require 'test_helper'

class ContactNotesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contact_note = contact_notes(:one)
  end

  test "should get index" do
    get contact_notes_url
    assert_response :success
  end

  test "should get new" do
    get new_contact_note_url
    assert_response :success
  end

  test "should create contact_note" do
    assert_difference('ContactNote.count') do
      post contact_notes_url, params: { contact_note: { contact_id: @contact_note.contact_id, content: @contact_note.content } }
    end

    assert_redirected_to contact_note_url(ContactNote.last)
  end

  test "should show contact_note" do
    get contact_note_url(@contact_note)
    assert_response :success
  end

  test "should get edit" do
    get edit_contact_note_url(@contact_note)
    assert_response :success
  end

  test "should update contact_note" do
    patch contact_note_url(@contact_note), params: { contact_note: { contact_id: @contact_note.contact_id, content: @contact_note.content } }
    assert_redirected_to contact_note_url(@contact_note)
  end

  test "should destroy contact_note" do
    assert_difference('ContactNote.count', -1) do
      delete contact_note_url(@contact_note)
    end

    assert_redirected_to contact_notes_url
  end
end
