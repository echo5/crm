require 'test_helper'

class EmailAccountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @email_account = email_accounts(:one)
  end

  test "should get index" do
    get email_accounts_url
    assert_response :success
  end

  test "should get new" do
    get new_email_account_url
    assert_response :success
  end

  test "should create email_account" do
    assert_difference('EmailAccount.count') do
      post email_accounts_url, params: { email_account: { imap_encryption: @email_account.imap_encryption, imap_host: @email_account.imap_host, imap_password: @email_account.imap_password, imap_port: @email_account.imap_port, imap_username: @email_account.imap_username, smtp_encryption: @email_account.smtp_encryption, smtp_host: @email_account.smtp_host, smtp_password: @email_account.smtp_password, smtp_port: @email_account.smtp_port, smtp_username: @email_account.smtp_username } }
    end

    assert_redirected_to email_account_url(EmailAccount.last)
  end

  test "should show email_account" do
    get email_account_url(@email_account)
    assert_response :success
  end

  test "should get edit" do
    get edit_email_account_url(@email_account)
    assert_response :success
  end

  test "should update email_account" do
    patch email_account_url(@email_account), params: { email_account: { imap_encryption: @email_account.imap_encryption, imap_host: @email_account.imap_host, imap_password: @email_account.imap_password, imap_port: @email_account.imap_port, imap_username: @email_account.imap_username, smtp_encryption: @email_account.smtp_encryption, smtp_host: @email_account.smtp_host, smtp_password: @email_account.smtp_password, smtp_port: @email_account.smtp_port, smtp_username: @email_account.smtp_username } }
    assert_redirected_to email_account_url(@email_account)
  end

  test "should destroy email_account" do
    assert_difference('EmailAccount.count', -1) do
      delete email_account_url(@email_account)
    end

    assert_redirected_to email_accounts_url
  end
end
